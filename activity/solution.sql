USE blog_db;

--
-- users table
--

INSERT INTO users (email, password) VALUES ('johnsmith@gmail.com', 'passwordA'); -- datetime value is on DEFAULT CURRENT_TIMESTAMP;

INSERT INTO users (email, password) VALUES ('juandelacruz@gmail.com', 'passwordB'); -- datetime value is on DEFAULT CURRENT_TIMESTAMP;

INSERT INTO users (email, password) VALUES ('janesmith@gmail.com', 'passwordC'); -- datetime value is on DEFAULT CURRENT_TIMESTAMP;

INSERT INTO users (email, password) VALUES ('mariadelacruz@gmail.com', 'passwordD'); -- datetime value is on DEFAULT CURRENT_TIMESTAMP;

INSERT INTO users (email, password) VALUES ('johndoe@gmail.com', 'passwordE'); -- datetime value is on DEFAULT CURRENT_TIMESTAMP;

--
-- posts table
--

INSERT INTO posts (title, content, author_id) VALUES ('First Code', 'Hello World!', 1); -- datetime value is on DEFAULT CURRENT_TIMESTAMP;

INSERT INTO posts (title, content, author_id) VALUES ('Second Code', 'Hello Earth!', 1); -- datetime value is on DEFAULT CURRENT_TIMESTAMP; 

INSERT INTO posts (title, content, author_id) VALUES ('Third Code', 'Welcome to Mars!', 2); -- datetime value is on DEFAULT CURRENT_TIMESTAMP;

INSERT INTO posts (title, content, author_id) VALUES ('Fourth Code', 'Bye bye solar system!', 4); -- datetime value is on DEFAULT CURRENT_TIMESTAMP;

--
-- GET all posts WHERE author_id = ?
--

SELECT * FROM posts WHERE author_id = 1;

--
-- GET all the users email and datetime
--

SELECT email, datetime_created FROM users;

--
-- UPDATE posts WHERE id = ?
--

UPDATE posts SET content = 'Hello to the people of the Earth' WHERE id = 2;

--
-- DELETE user using WHERE email = ?
--

DELETE FROM users WHERE email = 'johndoe@gmail.com'; -- WHERE id = 5;